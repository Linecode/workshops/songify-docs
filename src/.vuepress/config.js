const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: '.NET - Rest API',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: 'Workshop',
  
  base: '/workshops/songify-docs/',
  dest: 'public',

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#EE5141' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    nav: [
        { text: 'Przygotowanie Środowiska', link: '/enviroment/' },
        { text: 'Basic', link: '/part-1/' },
        { text: 'Advanced', link: '/part-2/' },
        { text: 'HATEOAS', link: '/part-3/' },
        { text: 'Materiały', link: '/part-4/' }
    ],
    sidebar: "auto"
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]
}
