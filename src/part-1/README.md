# Ćwiczenia podstawy WebAPI

## Ćwiczenie 1 - Serwer
Stwórz katalog roboczy, w którym wykonasz ćwiczenie. Wejdź do niego następującą komendą:
```
$ mkdir rest-api-workshop
$ cd rest-api-workshop
```

Zacznijmy od stworzenia pierwszej aplikacji konsolowej. Robimy to za pomocą komendy:
```
$ dotnet new webapi -n Songify.Minimal -o ./Songify.Minimal
```

Mozesz tez utworzyc solucje dla calego projektu

```
$ dotnet new sln -n Songify
```

Aby dodac wczesniej utworzony projekt do solucji

```
dotnet sln add **/*csproj
```

Następnie usuń wszystkie pliki `.cs` z projektu.
Stwórz nowy pusty plik `Program.cs`.

W pliku `Program.cs` dodaj następujący kod

```csharp
Host.CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder => {
       webBuilder.Configure(app => {
           app.UseRouting();
       }); 
    }).Build().Run();
```
Pamiętaj aby dodac odpowiednie instrukcje `using`. Jako ze pracujemy na platformie .NET 5 to nie musisz tworzyc klasy bazowej dla programu.

Następnie ponizej klauzuli `app.UseRouting()` dodaj definicję endpoint-ów.

```csharp
app.UseEndpoints(endpoints => {
    endpoints.MapGet("/", async context => {
        await context.Response.WriteAsJsonAsync(
            new { Id = 1, "Metallica" }
        );
    });
    
    endpoints.MapPost("/", async context => {
        var artist = await context.Request.ReadFromJsonAsync<Artist>();
        
        await context.Response.WriteAsJsonAsync(artist);
    });
});
```

Aby wszystko zadziałało poprawnie musisz jeszcze zdefiniowac encję `Artist`, wystarczy ze to będzie rekord.

```csharp
record Artist(int Id, string Name);
```

Mozesz tą encję wstawic po prostu na sam koniec tego pliku.

## Ćwiczenie 2 - Wstrzykiwanie Zaleznosci

Do wczesniejsz utworzonej aplikacji dodaj klasę o nazwie `InMemoryRepository`. Klasa powinna zawierac następujące metody: 

- `void Add(Artist artist)` -> Dodaje instację klasy artist do repozytorium. (Repozytorium na chwilę obecną mozesz zaimplementowac jako dodawanie elementow to pola o type `List<Artist>`)
- `Artist Get(int id)` -> Zwraca instację klasy Artist o zadanym id
- `void Delete(int id)` -> Kasuje z listy instancję klasy Artist o zadanym id
- `void Update(Artist artist)` -> Uaktualnia instacje klasy artist o zadanym id, w tym przypadku mozesz po prostu wywolac metody Delete i Update po sobie.

Bardzo zachęcam abys sam spróbował napisac tą klase. Umiesc ją na dole pliku. 
Jezeli będziesz miec jakies problemy to wteyd mozesz uzyc klasy przygotowej przezemnie

:::details
```csharp
class InMemoryRepository
{
    private readonly List<Artist> _artists = new();

    public void Add(Artist artist) => _artists.Add(artist);
    public IEnumerable<Artist> GetAll() => _artists;
    public Artist Get(int id) => _artists.FirstOrDefault(x => x.Id == id);
    public void Delete(int id) => _artists.Remove(Get(id));
    public void Update(Artist artist)
    {
        Delete(artist.Id);
        Add(artist);
    }
}
```
:::

Następnie na samym końcu pliku dodaj jeszcze jedną klasę, która będzie pomocna do odczytywania wartosci z route-a. 

```csharp
class GetsRoutesValueHelper
{
    public T Get<T>(HttpContext context, string paramName) where T : struct
    {
        var value = context.Request.RouteValues[paramName].ToString();
        if (string.IsNullOrWhiteSpace(value)) throw new ArgumentNullException(nameof(paramName));
        return (T)Convert.ChangeType(value, typeof(T));
    }
}
```

Następnie przejdź na górę pliku i powyzej `Host.CreateDefaultBuilder` utwórz obiekt klasy `ServiceCollection`

```csharp
var services = new ServiceCollection();
```

Następnie zarejestruj klasy `InMemoryRepository` i `GetsRoutesValueHelper` w kontenerze zaleznosci jako odpowiednio `Singleton` i `Transient`.

```csharp
services.AddTransient<GetsRoutesValueHelper>();
services.AddSingleton<InMemoryRepository>();
```

Następnie zbuduj instancję klasy `ServiceProvider` z obecnego kontenera zaleznosci.

```csharp
var serviceProvider = services.BuildServiceProvider();
```

Następnie przejdź do edycji endpointów. W kazdym endpoint-cie zacznij od utworzenia scope-a, kontenera zaleznosci: 

```csharp
endpoints.MapX("/artists/{id:int}", async context => {
    using var scope = serviceProvider.CreateScope();
});
```

W kolejnym kroku dodaj pobieranie obu wczesniej zdefiniowanych serwisow z kontenera zaleznosci

```csharp
var repository = scope.ServiceProvider.GetRequiredService<InMemoryRepository>();
var routeHelper = scope.ServiceProvider.GetRequiredService<GetsRoutesValueHelper>();
```

Następnie w kolejnym kroku spróbuj zaimplementowac 3 endpointy: 

- MapGet /artists/{id:int} -> Pobiera instancję obiektu artist z repozytorium i zwraca ją w postaci JSON-a.
- MapPost /artists -> Zapisuje nową instancję obiektu artist do repozytorium
- MapDelete /artists/{id:int} -> Kasuje instancje obiektu artists z repozytorium

::: tip
Aby zwrócic json-a z api wykorzystaj ponizszy kod: 

```csharp
await context.Response.WriteAsJsonAsync(artist);
```
:::

::: tip
Aby pobrac parametr z adresu url, wykorzystaj wczesniej utworzony serwis `GetsRoutesValueHelper`

```csharp
var id = routeHelper.Get<int>(context, "id");
```
:::

Ponizej znajdziesz gotowe rozwiazanie, jednak bardzo Cie zachęcam do spróbowania swoich sił samemu

::: details
```csharp
app.UseEndpoints(endpoints =>
{
    endpoints.MapGet("/artists/{id:int}", async context =>
    {
        using var scope = serviceProvides.CreateScope();
        var repository = scope.ServiceProvider.GetRequiredService<InMemoryRepository>();
        var routeHelper = scope.ServiceProvider.GetRequiredService<GetsRoutesValueHelper>();
            
        var id = routeHelper.Get<int>(context, "id");

        var artist = repository.Get(id);
        
        await context.Response.WriteAsJsonAsync(artist);
    });

    endpoints.MapPost("/artists", async context =>
    {
        var artist = await context.Request.ReadFromJsonAsync<Artist>();
        
        using var scope = serviceProvides.CreateScope();
        var repository = scope.ServiceProvider.GetRequiredService<InMemoryRepository>();
        
        repository.Add(artist);

        await context.Response.WriteAsJsonAsync(artist);
    });

    endpoints.MapDelete("/artists/{id:int}", async context =>
    {
        using var scope = serviceProvides.CreateScope();
        var repository = scope.ServiceProvider.GetRequiredService<InMemoryRepository>();
        var routeHelper = scope.ServiceProvider.GetRequiredService<GetsRoutesValueHelper>();
            
        var id = routeHelper.Get<int>(context, "id");
        
        repository.Delete(id);

        await context.Response.WriteAsJsonAsync(new {status = "ok"});
    });
});
```
:::

Jezeli udalo Ci się skończyc to zadanie przed grupą to spróbuj stworzyc endpoint do edycji artysty. 
Następnie spróbuj poekperymentowac z cyklem zycia obiektow w kontenerze DI.

## Ćwiczenie 3 - Middleware

W tym ćwiczeniu skupimy się teraz na tworzeniu Middleware-ów. Zacznij od stworzenia pierwszego bardzo przykładowego middleware-a. Umiesc jego definicje nad linijka `app.UseRouting`

```csharp
    app.Run(async context =>
    {
        await context.Response.WriteAsync("Hello .NET Core!");
    });
```

Jest to middleware z wykorzystaniem funkcji `Run` na obiekcie `IApplicationBuilder`. Tak jak to było powiedziane podczas prezentacji, wykonanie dodanego middleware kończy pipeline procesujący zadania HTTP na platformie .NET Core.

Przejdźmy zatem do poćwiczenia innych sposobów dodawania middleware-ów.

### App.Use

Zacznij od bardzo prostego przypadku. Na podstawie parametru z QueryString-a potrzebujmy ustawić `CultureInfo` w naszej aplikacji.

Przejdź do pliku `Program.cs`, w nim do metody `Configure` z obiektu `webBuilder`. To właśnie w niej odbywa się konfiguracja pipelin-a aplikacji. Najprostszym sposobem dodania własnego middleware jest wywołanie metody `app.Use(argument)`, która w swoim parametrze przyjmie wyrażenie lambda. Najprostsze takie wyrażenie wygląda następująco:

```csharp
app.Use((context, next) => { return next(); })
```

Taki middleware nic nie robi, przekazuje tylko wywołanie do kolejnego middlewar-a, aż w końcu zostanie wywołany ten middleware, zarejestrowany za pomocą metody `app.Run`.

Wracając do sedna problemu: zakładając, że otrzymamy informacje o kulturze w QueryString-u w kluczu `culture`, spróbuj napisać middleware, który ustawi odpowiednie propertisy w obiekcie `CultureInfo` (CurrentCulture i CurrentUICulture).
W formie przypomnienia, krótki snippet tworzący obiekt CultureInfo

```csharp
var culture = new CultureInfo("en");
```

Mam nadzieje, że udało Ci się. Jeżeli nie, to rozwiązanie znajdziesz poniżej:

```csharp
app.Use((context, next) =>
    {
        var cultureQuery = context.Request.Query["culture"];
        if (!string.IsNullOrWhiteSpace(cultureQuery))
        {
            var culture = new CultureInfo(cultureQuery);

            CultureInfo.CurrentCulture = culture;
            CultureInfo.CurrentUICulture = culture;
        }

        return next();
    });
```

Jeszcze, abyś mógł przetestować to rozwiązanie, dopisz ponizej kolejnego middleware-a, który wypisze `DisplayName` z cultureInfo.

```csharp
 app.Run(async (context) =>
    {
        await context.Response.WriteAsync(
            $"Hello {CultureInfo.CurrentCulture.DisplayName}");
    });
```

Teraz uruchom aplikację i sprawdź jak reaguje na różne przypadki wartości w kluczu `culture`. Możesz przetestować następujące kombinacje:
- en
- pl_PL
- en_US
- en_EN
- none
- null

### Enkapsulacja Middleware-a w oddzielnej klasie

Jako, że taka implementacja spowodowałaby bardzo duży rozrost pliku `Startup.cs`. Konwencją jest, aby zamykać middleware-y w osobnych klasach. Dlatego w projekcie utwórz katalog Middlewares, a w nim stwórz klasę `RequestCultureMiddleware`. Spróbuj, na podstawie slajdów, samemu napisać logikę tego middleware-a.

```csharp
public class RequestCultureMiddleware
{
    private readonly RequestDelegate _next;

    public RequestCultureMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        var cultureQuery = context.Request.Query["culture"];
        if (!string.IsNullOrWhiteSpace(cultureQuery))
        {
            var culture = new CultureInfo(cultureQuery);

            CultureInfo.CurrentCulture = culture;
            CultureInfo.CurrentUICulture = culture;

        }

        // Call the next delegate/middleware in the pipeline
        await _next(context);
    }
}
```

Inną, dobra praktyką, jest tworzenie extension method do rejestracji Middleware. Pozwoli to utrzymać czystość w pliku `Startup.cs` w metodzie `Configure`. Z wykorzystaniem extension method ta metoda będzie wyglądać następująco:

```csharp
app.UseMiddleware1();
app.UseMiddleware2();
app.UseMiddleware3();
```
Ewentualnie, będzie można to jeszcze bardziej skrócić, wykorzystując wzorzec chain of resposibility.

```csharp
app.UseMiddleware1()
    .UseMiddleware2()
    .UseMiddleware3();
```

W tym przypadku wystarczy Ci poniższa implementacja extension method:

```csharp
public static class RequestCultureMiddlewareExtensions
{
    public static IApplicationBuilder UseRequestCulture(
        this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<RequestCultureMiddleware>();
    }
}
```

Teraz, w pliku `Program.cs`, dodaj poniższą linijkę do metody Configure (przed linia app.UseMvc)

```csharp
app.UseRequestCulture();
```

## Ćwiczenie 4 - Routing / Controllers

Do wykonania tego cwiczenia bedziesz potrzebowac nowego projektu. Mozesz go wygenerowac za pomoca konsoli
lub za pomoca swojego IDE. Aby wygenerowac z konsoli uzyj polelcenia: 

```
$ dotnet new webapi -n Songify.Simple -o ./Songify.Simple
```

::: warning
Pamiętaj aby przejsc do katalogu w którym znajduje się solucja przed wykonaniem tego polecenia
:::

Następnie mozesz dodac tej projekt do solucji wywolujac:

```
$ dotnet sln add **/*csproj
```

### Models

W projekcie utwórz katalog `Models`. W utwórz encję `Artist`, która będzie zawierała takie propertisy jak: 

```
int Id

string Name

string Origin

DateTime? CreatedAt

bool? IsActive
```

Dodatkowo propertis Id mozesz udekorowac atrybutem: Key, natomiast propertis Name atrybutami Required i MaxLength(300).

Ponizej znajdziesz rozwiazanie: 
::: details
```csharp
public class Artist
{
    [Key]
    public int Id { get; set; }
    [Required]
    [MaxLength(300)]
    public string Name { get; set; }
    public string Origin { get; set; }
    public DateTime? CreatedAt { get; set; }
    public bool? IsActive { get; set; }
}
```
:::

### DTOS

Aby nie wystawiac naszego modelu w API, mozesz wykorzystac obiekty DTO (data transfer object). 
Na tych warsztatach będziesz potrzebowac następujących obiektów DTO

```csharp
public class CreateArtistResource
{
    [Required]
    [MaxLength(300)]
    public string Name { get; set; }
    
    public string Origin { get; set; }
    public DateTime? CreatedAt { get; set; }
    public bool? IsActive { get; set; }
}
```

```csharp
public class UpdateArtistResource : CreateArtistResource
{
    public int Id { get; set; }
}
```

Mozesz utworzyc na nie oddzielny katalog w projekcie i nazwac go `Dtos`.

### Mappings

Zeby uniknac recznego mapowania obiektow dto na model domenowy mozesz wykorzystac biblioteke AutoMapper.
Aby ją dodac do projektu zacznij od zainstalowania dwóch paczek nuget-a: 

- AutoMapper v10.1.1
- AutoMapper.Extensions.Microsoft.DependencyInjection v8.1.1

Nastepnie w pliku `Startup.cs` w metodzie `ConfigureServices` dodaj następujacy kod:

```csharp
public void ConfigureServices(IServiceCollection services)
{
    // ...
    services.AddAutoMapper(typeof(Startup));
    // ...
}
```

Nastepnie w projekcie utworz katalog `Mappings` i utworz w nim klasę `ArtistsProfiles`

```csharp
public class ArtistsProfiles : Profile
{
    public ArtistsProfiles()
    {
        CreateMap<CreateArtistResource, Artist>();
        CreateMap<UpdateArtistResource, Artist>();

        CreateMap<Artist, CreateArtistResource>();
        CreateMap<Artist, UpdateArtistResource>();
    }
}
```

### Controller

Kolejnym krokiem będzie utworzenie kontrolera `ArtistsController`. 
W kontrolerze zdefiniuj 4 metody: 

- `IActionResult Create(CreateArtistResource request)`
- `IActionResult Get(int id)`
- `IActionResult Remove(int id)`
- `IActionResult Update(UpdateArtistResource request)`

Na chwilę obecna nie musisz sie przejmowac przechowywaniem danych. Mozesz utworzyc statyczna list `List<Artist>`, ktora bedzie przechowywac liste artystow. 

::: tip
Pamietaj aby dodac Mapper-a jako parametr konstruktora

```csharp
private readonly IMapper _mapper;

public ArtistsController(IMapper mapper)
{
    _mapper = mapper;
}
```
::: 

Gotowe rozwiązanie znajdziesz ponizej: 

::: details
```csharp
[ApiController]
[Route("api/[controller]/")]
public class ArtistsController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly List<Artist> _artists = new();

    public ArtistsController(IMapper mapper)
    {
        _mapper = mapper;
    }

    [HttpPost]
    public async Task<IActionResult> Create(CreateArtistResource request)
    {
        var artist = _mapper.Map<CreateArtistResource, Artist>(request);
        
        _artists.Add(artist);

        return Created($"api/artists/{artist.Id}", request);
    }

    [HttpGet]
    [Route("{id}")]
    public async Task<IActionResult> Get(int id)
    {
        var artist = _artists.FirstOrDefault(x => x.Id == id);

        if (artist is null)
            return NotFound();
        
        return Ok(artist);
    }

    [HttpDelete]
    [Route("{id}")]
    public async Task<IActionResult> Remove(int id)
    {
        _artists.Remove(_artists.First(x => x.Id == id));

        // https://restfulapi.net/http-methods/#delete
        return NoContent();
    }
    
    [HttpPut]
    public async Task<IActionResult> Update(UpdateArtistResource request)
    {
        var artist = _artists.FirstOrDefault(x => x.Id == request.Id);

        if (artist is null)
            return NotFound();

        _artists.Remove(artist);
        _mapper.Map(request, artist);
        _artists.Add(artist);
        
        return Ok();
    }
}
```
:::

## Ćwiczenie 5 - Cusotomowe zachowania MVC

Zacznij od zmiany standardowych zachowan api. W pierwszej kolenosci zainstaluj paczkę nuget-a podmieniającą standardowy serializator JSON (System.Text.Json) na Newtonsoft.JSON. 

- Microsoft.AspNetCore.Mvc.Newtonsoft.Json v5.0.3

Następnie w pliku `startup.cs` dodaj następujący wpis

```csharp
services.AddControllers()
    .AddNewtonsoftJson(x =>
    {
        x.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        x.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        x.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
        x.SerializerSettings.Converters.Add(new StringEnumConverter());
    });
```

Następnie zdefiniuj rozszerzenie silnika generującego route-y z nazwy kontrolera / metody. 
To rozszerzenie pozwoli Ci na dokumentowanie Twojego API za pomocą notacji kebab-case. 

```csharp
public class SlugifyParameterTransformer : IOutboundParameterTransformer
{
    public string? TransformOutbound(object? value)
    {
        if (value == null) return null;

        return Regex.Replace(value.ToString() ?? string.Empty, "([a-z])([A-Z])", "$1-$2").ToLower();
    }
}
```

Aby wykorzystac te dwie dodane rzeczy udaj się do pliku `Startup.cs` i odszukaj linijkę `services.AddControllers` następnie podmień ją na kod ponizej

```csharp
services.AddControllers(options =>
    {
        options.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
    })
    .AddNewtonsoftJson(x =>
    {
        x.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        x.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        x.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
        x.SerializerSettings.Converters.Add(new StringEnumConverter());
    });
```


## Ćwiczenie 6 - Entity Framework / DAL

### DAL

W nastepnej kolejnosci musisz sie zajac DAL (Data access layer), to wlasnie tutaj bedzie kod komunikujacy sie z baza danych. 
Zacznij od dodania paczek nuget-a zwiazanych z Entity Frameworkiem

- Microsoft.EntityFrameworkCore v5.0.3
- Microsoft.EntityFrameworkCore.Design v5.0.3
- Microsoft.EntityFrameworkCore.SqlServer v5.0.3

Zainstalowac paczki mozesz albo za pomoca swojego IDE albo za pomoca polecenia konsoli

```
$ dotnet add package Microsoft.EntityFrameworkCore --version 5.0.3
```

W przyszlosci bedziesz musial wygenerowac migracje aby Entity Framework Core utworzyl baze danych i jej strukture na SqlServer. Najlepiej będzie to zrobic za pomoca dostepnego narzedzia cli, ktore mozesz zainstalowac za pomoca polecenia

```
$ dotnet tool install --global dotnet-ef
```

Nastepnie utwórz interfejs `IUnitOfWork` w katalogu `DAL` i wklej do niego ponizszą zawartosc

```csharp
    public interface IUnitOfWork : IDisposable
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
```

Następnie utwórz interfejs wskaznikowy `IRepository`

```csharp
public interface IRepository
{}
```

W kolejnym kroku musisz utworzyc `DBContext`, klase za pomoca, ktorej w kolejnych czesciach tego warsztatu bedziesz sie komunikowal z baza danych.

```csharp
public class SongifyDbContext : DbContext, IUnitOfWork
{
    public DbSet<Artist> Artists { get; set; }

    public SongifyDbContext(DbContextOptions<SongifyDbContext> options) : base(options)
    {
    }
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    { 
        optionsBuilder.UseLoggerFactory(MyLoggerFactory);
    }
    
    private static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder => { builder.AddConsole(); });
}
```

W nastepnej kolejnosci musisz utworzyc repozytorium artystów, którego zadaniem będzie enkapsulacja integracji z utworzonym DBContext-em. Repozytorium powinno zawierac te same metody co `InMemoryRepository` z wczesniejszych przykładów:

- void Add(Artist artist) -> Dodaje instację klasy artist do repozytorium. 
- Artist Get(int id) -> Zwraca instację klasy Artist o zadanym id
- void Delete(int id) -> Kasuje z listy instancję klasy Artist o zadanym id
- void Update(Artist artist) -> Uaktualnia instacje klasy artist o zadanym id

::: warning
Pamietaj aby wstrzyknac instancje klasy `SongifyDbContext` do repozytorium za pomoca konstruktora

```csharp
private readonly SongifyDbContext _context;
public ArtistsRepository(SongifyDbContext context)
{
    _context = context ?? throw new ArgumentNullException(nameof(context));
}
```
:::

Jedynym nowym elemenem w API repozytorum bedzie wystawienie instancji klasy DBContext do swiata zewnetrznego za pomoca interfejsu `IUnitOfWork`

Mozesz to zrobic za pomoca zwyklego propertis-a

```csharp
public IUnitOfWork UnitOfWork => _context;
```

Sprobuj najpierw samodzielnie na podstawie slajdow napisac repozytorium. Nastepnie sprawdz swoja implementacje z ponizszym przykladem

::: details
```csharp
public class ArtistsRepository : IRepository
{
    private readonly SongifyDbContext _context;

    public IUnitOfWork UnitOfWork => _context;
    
    public ArtistsRepository(SongifyDbContext context)
    {
        _context = context ?? throw new ArgumentNullException(nameof(context));
    }

    public void Add(Artist model)
    {
        _context.Artists.Add(model);
    }

    public Task<Artist> Get(int id)
    {
        return _context.Artists
            .FirstAsync(x => x.Id == id);
    }

    public void Update(Artist artist)
    {
        _context.Entry(artist).State = EntityState.Modified;
    }

    public void Remove(int id)
    {
        _context.Artists.Remove(new Artist {Id = id});
    }
}
```
:::

Z tak utworzonego repozytorium stworz interfejs `IArtistsRepository`. Będziesz go potrzebowac aby zarejestrowac repozytorium w kontenerze DI. Ten interfejs mozesz napisac ręcznie lub wykorzystac mozliwosci swojego IDE do wygenerowania takiego interfejsu albo skopiowac ponizszy kod

```csharp
public interface IArtistsRepository
{
    IUnitOfWork UnitOfWork { get; }
    void Add(Artist model);
    Task<Artist> Get(int id);
    void Update(Artist artist);
    void Remove(int id);
}
```

Następnie przejdź do apliku `appsettings.json` aby dodac konfirugację połączenia z bazą danych (tzw. ConnectionString)

```json
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "DefaultConnection": "Server=localhost,1433;Database={NazwaBazydanych};User Id={Uzytkownik};Password={Haslo}"
  }
```

Jako nazwe bazy danych mozesz wybrac wartosc `Songify.Simple`. Nazwę uzytkownika, haslo i adres serwera SqlServer musisz dopasowac do loklanych ustawień. Poniezej masz napisane kilka mozeliwosci: 

- Docker container: `Server=localhost,1433;Database=SongifySimple;User Id=SA;Password=yourStrong(!)Password`
- VisualStudio localdb: `Server=(localdb)\\mssqllocaldb;Database=SongifySimple;Trusted_Connection=True;MultipleActiveResultSets=true`

W następnym kroku udaj się do pliku `Statup.cs` aby w sekcji `ConfigureServices` dodac rejestracje DbContext-u w kontenerze zaleznosci.

```csharp
public void ConfigureServices(IServiceCollection services)
{
    services.AddDbContext<SongifyDbContext>(options =>
        options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
            conf =>
            {
                conf.MigrationsAssembly(typeof(Startup).Assembly.FullName);
            }));
    // ...
    services.AddControllers();
}
```

Ponizej dodaj rejestracje wszystkich klas implementujacych interfej `IRepository` za pomoca biblioteki `Scrutor`

```csharp
services.Scan(scan => scan.FromAssemblyOf<Startup>()
    .AddClasses(@class => @class.AssignableTo<IRepository>())
    .AsImplementedInterfaces());
```

Kiedy to wszystko bedzie gotowe przejdz do wygenerowania migracji EF Core. W konsoli przejdz do katalogu z projektem. 

::: warning
Bardzo wazne jest aby byc w katalogu z projektem a nie katalogu z solucja. Inaczej migracja nie zadziala.
:::

Nastepnie wykonaj polecenie

```
$ dotent ef migrations add InitialMigration -v
```

Nastepnia za pomoca ponizszego polecenia wygeneruj baze danych

```
$ dotnet ef database update -v
```

Sprawdz za pomoca IDE do bazy danych czy wszystko wygenerowalo sie prawidlowo.

### Integracja DAL z kontrolerami

Aby zintegrowac DAL z kontrolerem musisz w pierwszym kroku wstrzyknac instancje repozytorium do kontrolera.

```csharp
private readonly IArtistsRepository _repository;
private readonly IMapper _mapper;

public ArtistsController(IArtistsRepository repository, IMapper mapper)
{
    _repository = repository;
    _mapper = mapper;
}
```

Następnie musisz podmienic wszystkie odwolania do statycznej Listy na odwolania do repozytorium.
Gotowe rozwiazanie znajdziesz ponizej

::: details
```csharp
[ApiController]
[Route("api/[controller]/")]
public class ArtistsController : ControllerBase
{
    private readonly IArtistsRepository _repository;
    private readonly IMapper _mapper;

    public ArtistsController(IArtistsRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    [HttpPost]
    public async Task<IActionResult> Create(CreateArtistResource request)
    {
        var artist = _mapper.Map<CreateArtistResource, Artist>(request);
        
        _repository.Add(artist);
        await _repository.UnitOfWork.SaveChangesAsync();

        return Created($"api/artists/{artist.Id}", request);
    }

    [HttpGet]
    [Route("{id}")]
    public async Task<IActionResult> Get(int id)
    {
        var artist = await _repository.Get(id);

        if (artist is null)
            return NotFound();
        
        return Ok(artist);
    }

    [Authorize]
    [HttpDelete]
    [Route("{id}")]
    public async Task<IActionResult> Remove(int id)
    {
        _repository.Remove(id);
        await _repository.UnitOfWork.SaveChangesAsync();

        // https://restfulapi.net/http-methods/#delete
        return NoContent();
    }
    
    [HttpPut]
    public async Task<IActionResult> Update(UpdateArtistResource request)
    {
        var artist = await _repository.Get(request.Id);

        if (artist is null)
            return NotFound();

        _mapper.Map(request, artist);
        
        _repository.Update(artist);
        await _repository.UnitOfWork.SaveChangesAsync();

        return Ok();
    }
}
```
:::

## Ćwiczenie 7 - Open API / Swagger

Jako ze wygenerowałes projekt typu `webapi` to template tego projektu zawierał juz bardzo podstawową implementację Swagger-a. Pora ją rozszerzyc! 

W pliku `Startup.cs` odnajdź linijkę `services.AddSwaggerGen(//...`

Zacznij od dodania meta danych na temat projektu, który robisz. W ponizszym kodzie zedytuj wartosci tak aby bardziej odzwierciedlaly rzeczywistosc.

Ponizej wywołania metody `SwaggerDoc` dodaj jeszcze import komentarzy z pliku `xml`.
```csharp
services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Songify.Api", 
        Version = "v1",
        Description = "Dokumentacja api Songify",
        TermsOfService = new Uri("https://songify.com/termsofservice.pdf", UriKind.Absolute),
        Contact = new OpenApiContact
        {
            Name = "Songify Support",
            Email = "twojemail@example.com",
            Url = new Uri("https://example.com", UriKind.Absolute)
        }
    });
    
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    c.IncludeXmlComments(xmlPath);
});
```

::: warning
Pamiętaj o włączeniu generowania dokumentacji na podstawie komentarzy w kodzie, w propertisach projektu!
:::

Następnie w metodzie `Configure` dodaj middleware-y swagger-a zaraz po wywołaniu `UseDeveloperExceptionPage`

```csharp
app.UseSwagger();
app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Songify.ApiEndpoints v1"));
```

Następnie uruchom projekt i sprawdź czy pod adresem [https://localhost:5001/swagger/](https://localhost:5001/swagger/) Znajduje się dokumentacja API.

::: warning
Osoby korzystające z Visual Studio i IIS Expres będą miały podpięty inny port dla aplikacji, pamiętajcie o skorygowaniu tego w zapytaniu.
:::

Następnie udekoruj `ArtistsController` komentarzami i atrybutami tak jak na ponizszym przykładzie.

```csharp
[ApiController]
[Route("api/[controller]/")]
[Consumes(System.Net.Mime.MediaTypeNames.Application.Json)]
public class ArtistsController : ControllerBase
{
    private readonly IArtistsRepository _repository;
    private readonly IMapper _mapper;

    public ArtistsController(IArtistsRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> Create(CreateArtistResource request)
    {
        var artist = _mapper.Map<CreateArtistResource, Artist>(request);
        
        _repository.Add(artist);
        await _repository.UnitOfWork.SaveChangesAsync();

        return Created($"api/artists/{artist.Id}", request);
    }

    /// <summary>
    /// Get a specific Artist 
    /// </summary>
    /// <remarks>
    /// Some more comment inside remarks tag
    /// </remarks>
    /// <param name="id">Artist's id</param>
    /// <returns>An Artist Entity</returns>
    /// <response code="200">Returns existing artists entity</response>
    [HttpGet]
    [Route("{id}")]
    [Produces(System.Net.Mime.MediaTypeNames.Application.Json)]
    [ProducesResponseType(typeof(Artist), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Get(int id)
    {
        var artist = await _repository.Get(id);

        if (artist is null)
            return NotFound();
        
        return Ok(artist);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [Authorize]
    [HttpDelete]
    [Route("{id}")]
    public async Task<IActionResult> Remove(int id)
    {
        _repository.Remove(id);
        await _repository.UnitOfWork.SaveChangesAsync();

        // https://restfulapi.net/http-methods/#delete
        return NoContent();
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut]
    public async Task<IActionResult> Update(UpdateArtistResource request)
    {
        var artist = await _repository.Get(request.Id);

        if (artist is null)
            return NotFound();

        _mapper.Map(request, artist);
        
        _repository.Update(artist);
        await _repository.UnitOfWork.SaveChangesAsync();

        return Ok();
    }
}
```

Ponownie uruchom aplikację i zobacz jak zmienił się wygląd panelu swagger-a. 

## Cwiczenie 8 - Walidacja

Do pliku dto zawierającego klasę `CreateAartistResource` dodaj atrybuty `DataAnotation`. Mozesz wykorzystac te dwa, ktore byly uzyte w częsci teoretycznej: 

- Required
- MaxLength

Oprocz tego cala liste dostepnych atrybutow mozesz znalezc w tym miejscu: 
[https://docs.microsoft.com/en-us/dotnet/api/system.componentmodel.dataannotations?view=net-5.0](https://docs.microsoft.com/en-us/dotnet/api/system.componentmodel.dataannotations?view=net-5.0)

Oprocz tego dodaj implementacje interfejsu `IValidatableObject`, ktory wymusi zaimplementowanie w tej klasie metody Validate. W ciele tej metody dodaj sprawdzenie czy pole `CreateAt` nie zawiera daty przyszlej.

Ponizej znajdziesz gotowe rozwiazanie

::: details
```csharp
public class CreateArtistResource : IValidatableObject
{
    [Required]
    [MaxLength(300)]
    public string Name { get; set; }
    
    public string Origin { get; set; }
    public DateTime? CreatedAt { get; set; }
    public bool? IsActive { get; set; }
    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
        if (CreatedAt > DateTime.UtcNow)
        {
            yield return new ValidationResult("The Created At should not be in future", 
                new [] { nameof(CreatedAt) });
        }
    }
}
```
:::

Niestety po dodaniu `DataAnnotations` aplikacja zwraca odpowiedź 400 dla request-u, który zawiera niepoprawne dane. Zgodnie z tym co rozmawialismy podczas czesci teoretycznej lepiej w tym miejscu pasuje odpowiedz 422. Aby "zmusic" MVC do zwracania takiego kodu bledu musisz w pliku `Startup.cs` wykorzystac mechanizm pozwalajacy na nadpisanie metody generujacej `problemDetails`. W tym celu sprobujemy oprzec naszą implementację na specyfikacji rfc 7807.

::: details
```csharp
services.AddControllers(options =>
{
    options.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
})
.ConfigureApiBehaviorOptions(setupAction =>
{
    setupAction.InvalidModelStateResponseFactory = context =>
    {
        var problemDetailsFactory = context.HttpContext.RequestServices
            .GetRequiredService<ProblemDetailsFactory>();
        var problemDetails =
            problemDetailsFactory.CreateValidationProblemDetails(context.HttpContext,
                context.ModelState);

        problemDetails.Detail = "See the errors field for details";
        problemDetails.Instance = context.HttpContext.Request.Path;

        var actionExecutingContext = context as ActionExecutingContext;

        if ((context.ModelState.ErrorCount > 0) && (actionExecutingContext?.ActionArguments.Count() ==
                                                    context.ActionDescriptor.Parameters.Count))
        {
            // https://datatracker.ietf.org/doc/html/rfc7807
            problemDetails.Type = "https://songify.com/modelvalidationproblem";
            problemDetails.Status = StatusCodes.Status422UnprocessableEntity;
            problemDetails.Title = "One or more validation errors occured.";

            return new UnprocessableEntityObjectResult(problemDetails)
            {
                ContentTypes = {"application/problem+json"}
            };
        }

        problemDetails.Status = StatusCodes.Status400BadRequest;
        problemDetails.Title = "One or more errors on input occured";

        return new BadRequestObjectResult(problemDetails)
        {
            ContentTypes = {"application/problem+json"}
        };
    };
});
```
:::

## Cwiczenie 9 - Paginacja

Zacznij od utworzenia Endpoint-a, ktory bedzie zwracal wszystkich artystow. W tym celu w repozytorium utworz metode, `Task List<List<Artist>> GetArtists` w ktorej wywolasz nastepujacy kod: 

```csharp
return _context.Artists.ToListAsync();
```

::: tip
Pamietaj o zadeklarowaniu tej metody tez w interfejsie `IArtistsRepository`
:::

Następnie dodaj kolejny endpoint do kontrolera `ArtistsController`, w którym wywołasz metodę `repository.GetArtists`.



Nastepnie przejdz do implementacji generycznej klasy pomocniczej o nazwie `PagedList<T>`.
Klasa powinna zawierac takie pola jak

- `int CurrentPage` -> Obecny numer strony
- `int totalPages` -> Calkowita liczba stron
- `int PageSize` -> Ilosc elementow na pojedynczej podstronie
- `int TotalCount` -> Ilosc elementow
- `bool HasPrevious` -> Czy jest mozliwosc otrzymania wczesniejszej strony
- `bool HasNext` -> Czy jest mozliwosc otrzymania nastepnej strony

Klasa powinna dziedziczyc po klasie `List<T>`

Dodatkowo dodaj statyczna metode wytworcza o nazwie `Create` i sygnaturze: 
`Task<PagedList<T>> Create(IQueryable<T> source, int pageNumber, int pageSize)`;

W ciele tej metody wytworczej, uzywajac metod LINQ Skip i Take utworz funkcje ktora bedzie odpowiedzialna za paginacje wynikow zbioru IQueryable. 

::: details
```csharp
public class PagedList<T> : List<T>
{
    public int CurrentPage { get; private set; }
    public int TotalPages { get; private set; }
    public int PageSize { get; private set; }
    public int TotalCount { get; private set; }
    public bool HasPrevious => (CurrentPage > 1);
    public bool HasNext => (CurrentPage < TotalPages);

    public PagedList(List<T> items, int pageNumber, int pageSize, int count)
    {
        CurrentPage = pageNumber;
        PageSize = pageSize;
        TotalCount = count;
        TotalPages = (int)Math.Ceiling(count / (double)pageSize);

        AddRange(items);
    }

    public static async Task<PagedList<T>> Create(IQueryable<T> source, int pageNumber, int pageSize)
    {
        var count = source.Count();
        var items = await source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();

        return new PagedList<T>(items, pageNumber, pageSize, count);
    }
}
```
:::

Przejdz do sekcji Dtos i dodaj nowy katalog o nazwie `ResourceParameters`
Wewnętrz tego katalogu utwórz klasę `ArtistResourceParameter`.
Klasa ma zawierac takie pola jak: 

- `int PageNumber`
- `int PageSize`

Z czego PageSize nie moze byc wiekszy niz 20, natomiast defaultowa wartoscia pola `PageSize` powinno byc 10.

::: details
```csharp
public class ArtistResourceParameters
{
    private const int MaxPageSize = 20;

    public int PageNumber { get; set; } = 1;
    private int _pageSize = 10;
    public int PageSize
    {
        get => _pageSize;
        set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
    }
}
```
:::

Przejdz do sekcji DAL i w interfejsie `IArtistsRepository` zaktualizuj deklarację metody: 

```csharp
Task<PagedList<Artist>> GetArtists(ArtistResourceParameters artistResourceParameters);
```

Następnie przejdź do implementacji tego interfejsu i zaktualizuj implementacje wczesniej zadeklarowanej metody

```csharp
public Task<PagedList<Artist>> GetArtists(ArtistResourceParameters artistResourceParameters)
{
    var collection = _context.Artists.AsQueryable();
    
    return PagedList<Artist>.Create(collection, artistResourceParameters.PageNumber, artistResourceParameters.PageSize);
}
```

Następnie przejdz do kontrolera i dodaj endpoint, ktory bedzie przyjmowac obiek klasy `ArtistResourceParameter` jako swój parametr i przekazywac go do repozytorium.

::: tip
Uzyj sposobu bindowania danych `FromQuery`
:::

Wewnatrz tej akcji wywołaj metodę `_repository.GetArtists(artistResourceParameter)` i na podstawie jej wyniku stwórz anonimowy obiekt, ktory bedzie zawieral metadane paginacji.

```csharp
var paginationMetadata = new
{
    totalCount = artists.TotalCount,
    totalPages = artists.TotalPages,
    currentPage = artists.CurrentPage,
    pageSize = artists.PageSize
};
```

Następnie dodaj zserializowany obiekt jako naglówek odpowiedzi HTTP (X-Pagination). 

```csharp
Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(paginationMetadata));
```

Pelna implementacje wraz z atrybutami swaggera znajdziesz ponizej

::: details
```csharp
/// <summary>
/// Get a list of Artist 
/// </summary>
/// <remarks>
/// Some more comment inside remarks tag
/// </remarks>
/// <returns>List of artists</returns>
/// <response code="200">Returns existing artists entity</response>
[HttpGet]
[HttpHead]
[Produces(System.Net.Mime.MediaTypeNames.Application.Json)]
[ProducesResponseType(typeof(PagedList<Artist>), StatusCodes.Status200OK)]
[ProducesResponseType(StatusCodes.Status404NotFound)]
public async Task<IActionResult> GetListOfArtists([FromQuery] ArtistResourceParameters artistResourceParameters)
{
    var artists = await _repository.GetArtists(artistResourceParameters);

    var paginationMetadata = new
    {
        totalCount = artists.TotalCount,
        totalPages = artists.TotalPages,
        currentPage = artists.CurrentPage,
        pageSize = artists.PageSize
    };
    
    Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(paginationMetadata));
    
    return Ok(artists);
}
```
:::

## Cwiczenie 10 - Filtrowanie i przeszukiwanie

Dodaj dwa nowe pola do obiektu `ArtistResourceParameters`

- `bool? IsActive`
- `string SearchQuery`

Następnie w repozytorium, w metodzie do pobierania listy artystów dodaj odpowiednie klauzule `where`.

```csharp
if (artistResourceParameters.IsActive.HasValue)
{
    collection = collection.Where(x => x.IsActive == artistResourceParameters.IsActive);
}

if (!string.IsNullOrWhiteSpace(artistResourceParameters.SearchQuery))
{
    var searchQuery = artistResourceParameters.SearchQuery.Trim();
    collection = collection.Where(x => x.Name.Contains(searchQuery) || x.Origin.Contains(searchQuery));
}
```

Po zmianach uruchom aplikacje ponownie i przetestuj zmiany.

## Cwiczenie 11 - Auth

### Timestamp util

Zacznij od stworzenia katalogu `Utils`. Następnie stwórz plik `DateTimeExtensions.cs` i skopiuj do niego ponizsza klasę :

```csharp
public static class DateTimeExtensions
{
    public static long ToTimestamp(this DateTime dateTime)
    {
        var centuryBegin = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        var expectedDate = dateTime.Subtract(new TimeSpan(centuryBegin.Ticks));

        return expectedDate.Ticks / 10000;
    }
}
```
### JwtService

Nastepnie przejdź do implementacji serwisu odpowiedzialnego za uwierzytelnianie uzytkowników. 
Utwórz w projekcie katalog `Services`. Następnie w jego srodku utwórz katalog `IdentityService`.

Potem dodaj do niego klasę `JsonWebToken`, jej implementację znajdziesz ponizej

```csharp
public class JsonWebToken
{
    public string AccessToken { get; set; }
    public string RefreshToken { get; set; }
    public long Expires { get; set; }
    public IDictionary<string, string> Claims { get; set; }
    public int UserId { get; set; }
}
```

Następnie dorzuc kolejną klasę, która posłuzy jako obiekt DTO dla kontrolera odpowiedzialnego za logowanie.

```csharp
public class SignInResource
{
    [JsonProperty(PropertyName = "email")]
    public string Email { get; set; }
    [JsonProperty(PropertyName = "password")]
    public string Password { get; set; }
}
```

W kolejnym kroku dodaj ostatnią klasę znajdującą się w tym serwisie. Jest to klasa `JwtHandler`. 
Ta implementacja posłuzy Ci jako mock do generowania tokenów JWT. Jako ze jest to dosc prosta w uzyciu klasa to w przyszlosci bedziesz mogl jej uzyc aby np. generowac tokeny JWT dla biblioteki .NET Identity.

```csharp
public class JwtHandler
{
    private readonly SigningCredentials _signingCredentials;

    public JwtHandler()
    {
        var issuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("JLBMU2VbJZmt42sUwByUpJJF6Y5mG2gPNU9sQFUpJFcGFJdyKxskR3bxh527kax2UcXHvB"));
        _signingCredentials = new SigningCredentials(issuerSigningKey, SecurityAlgorithms.HmacSha256);
    }

    public JsonWebToken CreateToken(int userId, string email, IDictionary<string, string> claims = null, string refreshToken = "")
    {
        var now = DateTime.UtcNow;
        var jwtClaims = new List<Claim>
        {
            new Claim(JwtRegisteredClaimNames.Sub, userId.ToString()),
            new Claim(JwtRegisteredClaimNames.UniqueName, email),
            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            new Claim(JwtRegisteredClaimNames.Iat, now.ToTimestamp().ToString()),
        };

        var customClaims = claims?.Select(claim => new Claim(claim.Key, claim.Value)).ToArray()
                            ?? Array.Empty<Claim>();
        jwtClaims.AddRange(customClaims);
        var expires = now.AddMinutes(60);
        var jwt = new JwtSecurityToken(
            issuer: "Songify.Identity",
            claims: jwtClaims,
            notBefore: now,
            expires: expires,
            signingCredentials: _signingCredentials
        );
        var token = new JwtSecurityTokenHandler().WriteToken(jwt);

        return new JsonWebToken
        {
            UserId = userId,
            AccessToken = token,
            RefreshToken = refreshToken,
            Expires = expires.ToTimestamp(),
            Claims = customClaims.ToDictionary(c => c.Type, c => c.Value)
        };
    }
}
```

Następnie przejdź do pliku `Startup.cs` aby wykonac kilka zmian. 

Po pierwsze w metodzie `ConfigureServices` zarejestruj klase `JwtHandler` jako transient. 

Następnie w tej samej metodzie dodaj konfigurację uwierzytelnienia za pomocą tokenów JWT. 

```csharp
services.AddAuthentication(opt =>
{
    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer(cfg =>
{
    cfg.TokenValidationParameters = new TokenValidationParameters
    {
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("JLBMU2VbJZmt42sUwByUpJJF6Y5mG2gPNU9sQFUpJFcGFJdyKxskR3bxh527kax2UcXHvB")),
        ValidIssuer = "Songify.Identity",
        ValidAudience = "",
        ValidateAudience = false,
        ValidateLifetime = true,
        ClockSkew = TimeSpan.Zero
    };
});
```

::: danger
Jak mozesz zauwazyc spora czest konfiguracja sie pokrywa, oprocz tego klucz, który jest wykorzystywany do generowania podpisu token-ów JWT jest zahardkodowany na sztywno w kodzie. To jest zla praktyka! 
Niestety ograniczenia czasowe na tych warsztatach nie pozwalają nam na pokrycie takich tematów jak zarządzanie sekretami czy wstrzykiwanie konfiguracji przez systemy CI / CD. 
:::

- *Zadanie dla Chętnych: Wykorzystaj mechanizm options: [Dokumentacja](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/options?view=aspnetcore-5.0) aby przeniesc konfiguracje generowania kluczy JWT do pliku appsettings.json

Nie zapomnij jeszcze dodac `UseAuthentication` i `UseAuthorization` w metodzie `Configure`

```csharp
app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});
```

### Identity Controller

Następny elementem układanki będzie dodanie Kontrolera, który będzie odpowiedzialny za uwierzytelnienie uzytkownika.
W katalogu `Controllers` utwórz klasę `IdentityController`.
- IdentityController w swoim konstrukturze powinien przyjmowa obiekt klasy `JwtHandler`.
- IdentityController powinien posiadac akcję Index z parametrem `SignInResource resource`
- Akcja index powinna byc dostępna pod adresem `api/identity/sing-in`
- W ciele akcji index, powinno nastąpic wywołanie metody `CreateToken` z paramterami 1,request.Email

::: details
```csharp
[ApiController]
public class IdentityController : ControllerBase
{
    private readonly JwtHandler _jwtHandler;

    public IdentityController(JwtHandler jwtHandler)
    {
        _jwtHandler = jwtHandler;
    }

    [HttpPost]
    [Route("api/[controller]/[action]")]
    [ActionName("sing-in")]
    public IActionResult Index(SignInResource request)
    {
        var token = _jwtHandler.CreateToken(1, request.Email);

        return Ok(token);
    }
}
```
:::

Kolejnym elementem w tym cwiczeniu będzie oznaczenie endpoint-a tak aby wymusic na nim uwierzytelnienie.
Niech takim endpointem schowanym za uwierzytelnieniem będzie endpoint do kasacji artystów.

::: details
```csharp
[Authorize]
[HttpDelete]
[Route("{id}")]
public async Task<IActionResult> Remove(int id)
{
    _repository.Remove(id);
    await _repository.UnitOfWork.SaveChangesAsync();

    // https://restfulapi.net/http-methods/#delete
    return NoContent();
}
```
:::

Następnie musisz zdefiniowac w deklaracji swagger-a z jakiego uwierzytelnienia ma korzystac. W tym przypadku będzie do tzw. Bearer Token przekazywany w nagłówku `Authorization` zadania HTTP.
Aby to skonfigurowac dodaj ponizszy kod ponizej wywołania funkcji: `c.SwaggerDoc`.

```csharp
c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme {
    In = ParameterLocation.Header, 
    Description = "Please insert JWT with Bearer into field",
    Name = "Authorization",
    Type = SecuritySchemeType.ApiKey 
});
c.AddSecurityRequirement(new OpenApiSecurityRequirement {
    { 
        new OpenApiSecurityScheme 
        { 
            Reference = new OpenApiReference 
            { 
                Type = ReferenceType.SecurityScheme,
                Id = "Bearer" 
            } 
        },
        Array.Empty<string>()
    }
});
```

Następnie uruchom projekt i ponownie wejdź na podstronę Swagger-a. Po prawej stronie nad listą endpoint-ów powinien pojawic się duzy przycisk `Authorize`.
Aby przetestowac czy caly proces uwierzytelnienia dziala poprawnie: 
- Przejdz w swagerze do endpointu z identity. 
- Wygeneruj access token. 
- Wroc do przycisku Authorize
- W oknie dialogowym wpisz `Bearer ACCESS_TOKEN` (zastap "ACCESS_TOKEN" swoim swiezo utworzyonym tokenem")

## Ćwiczenie 12 - Cache

Zarządzanie cach-em jest jednym z trudniejszych wyzwań programistycznych. Na szczęscie obecne przeglądarki internetowe bardzo dobrze wspierają nas programistów w tym zadaniu. 
Aby jak najbardziej ułatwic sobie zadanie zacznij od doinstalowania paczki nuget-a. 

- Marvin.Cache.Headers v5.0.1

Następnie w pliku `Startup.cs` dodaj ponizszą konfigurację. 

```csharp
services.AddHttpCacheHeaders((expirationModelOptions =>
{
    expirationModelOptions.MaxAge = 60;
    expirationModelOptions.CacheLocation = CacheLocation.Private;
}), (validationModelOptions) =>
{
    validationModelOptions.MustRevalidate = true;
});
```

Ta konfiguracja będzie zaaplikowano globalnie do wszystkich zasobów. Jezeli chcesz ją nadpisac na jakims endpoincie to mozesz do tego wykorzystac atrybuty dostepne w bibliotece.
Przykład ponizej:  

```csharp
/// <summary>
/// This is a summary of a method
/// </summary>
/// <param name="request"></param>
/// <returns></returns>
[HttpPost]
[HttpCacheValidation(NoCache = true)]
[Route("api/[controller]/[action]")]
[ActionName("sing-in")]
public IActionResult Index(SignInResource request)
{
    var token = _jwtHandler.CreateToken(1, request.Email);

    return Ok(token);
}
```