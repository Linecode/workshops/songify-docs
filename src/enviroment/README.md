# Przygotowanie Środowiska
Cześć,
Pierwszą i najważniejszą informacją jest fakt, że na warsztatach będziecie potrzebować własnego sprzętu.

W tej sekcji dowiesz się jak poprawnie skonfigurować środowisko developerskie, na którym będziemy pracowali podczas warsztatów.
## Oprogramowanie
Przyda się na pewno:
- IDE jak Visual Studio Code / Visual Studio / Rider. Szkoleniowiec podczas ćwiczeń będzie używać Rider-a.
- IDE dla bazy danych jak np. Sql Server Management Studio / Data Grip. Szkoleniowiec podczas ćwiczeń będzie uzywać JetBrains Data Grip
- [Postman](https://www.getpostman.com/)
- Przeglądarka internetowa (Chrome / Firefox)
- Baza Danych Sql Server
## Postman
Podczas warsztatów bedą wykonywane szyfrowane połaczenia lokalne (https i wss). Koniecznym jest skonfigurowanie Postman-a, aby nie walidował certyfikatów SSL. Wystarczy wejsć w ustawienia Postman-a i odznacz zaznaczoną na poniższym obrazku opcję.
![An image](https://linecode.pl/workshops/assets/postman.png)
## .NET
Podstawą do uczestnictwa w warsztatach jest posiadanie zainstalowanej platformy .NET Core w wersji 5.0+ na swoim komputerze. Potrzebne SDK możecie znaleźć tutaj:
[https://dotnet.microsoft.com/download](https://dotnet.microsoft.com/download)
### Testowy Projekt
Po zainstalowaniu .NET SDK uruchom kilka komend w konsoli, aby sprawdzić czy wszystko działa poprawnie.
 
Zacznijmy od sprawdzenia dotnet CLI, polecenie:
```
dotnet –-version
```
Powinno wypisać na konsoli obecnie zainstalowaną wersję dotnet-a np. (5.0.100)
Następnie sprawdź, czy jesteś w stanie wygenerować nowy projekt wraz z solucją. W przypadku używania konsoli git bash lub systemu linux / mac os to gotowy skrypt masz poniżej. 
``` 
cd ./workspace
mkdir testProject
cd testProject
dotnet new sln
mkdir src
dotnet new mvc -n TestProject -o ./src/TestProject
dotnet sln add **/*csproj
dotnet restore
dotnet build
cd ./src/TestProject
dotnet run
```
Jeżeli jesteś użytkownikiem Windows-a polecam wygenerowanie projektu i uruchomienie go za pomocą Visual Studio.
 
### Instalcja certyfikatów SSL
Instalacja wymaga posiadania certyfikatów SSL na swoim urządzeniu.  Wykorzystaj dotnet CLI.
```
dotnet dev-certs https --trust
```
Po wykonaniu tej komendy zostaniesz poproszony o potwierdzenie, czy na pewno chcesz dodać certyfikat. Ponieważ systemy operacyjne nie będą mogły zweryfikować poprawności certyfikatu dla domeny „localhost”, po zaakceptowaniu komunikatu i zrestartowaniu aplikacji, powinna się ona odpalić już pod bezpiecznym połączeniem po protokole https.

![An Image](https://linecode.pl/workshops/assets/ssl.png)
## Baza Danych
Do przejścia tych warsztatów będziesz potrzebować bazy danych SqlServer 2017+. 
Moze to być localDB zainstalowana razem z Visual Studio.

Innym sposobem na postawienie lokalnej bazy danych jest uruchomienie oficjalnego kontenera. Instrukcje jak to zrobić mozna znaleźć [tutaj](https://bd90.pl/sqlserver-na-mac-os-x/)

Oprócz samej bazy danych przyda się tez jakieś IDE do bazy np.
- Sql Server Management Studio ([Do Ściągnięcia tutaj](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15))
- JetBrains Data Grip 
