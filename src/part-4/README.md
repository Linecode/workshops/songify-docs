# Materiały

- Repozytorium gitlab-a: [https://gitlab.com/Linecode/workshops/songify](https://gitlab.com/Linecode/workshops/songify)
- Prezentacja: [http://bd90.pl/wp-content/uploads/2021/06/NET-Rest-API-v3.pdf](http://bd90.pl/wp-content/uploads/2021/06/NET-Rest-API-v3.pdf)
- Link do ankiety z ocena: [https://bit.ly/3zBMqsQ](https://bit.ly/3zBMqsQ)



## Mała reklama:

- Mój blog: [bd90.pl](https://bd90.pl)
- Mój profil na LN: [bd-90](https://www.linkedin.com/in/bd-90/)
- Mój profil na Twitter: [_bd_90](https://twitter.com/_bd_90)

## Odnosniki z prezentacji

- Dokument Sekuraka o bezpieczeństwie JWT [https://bit.ly/2tv5KqP](https://bit.ly/2tv5KqP)
- Biblioteka Scrutor [https://www.nuget.org/packages/Scrutor](https://www.nuget.org/packages/Scrutor)
- Ksiązka Dependency Injection Principles, Practices and Patterns [https://www.manning.com/books/dependency-injection-principles-practices-patterns](https://www.manning.com/books/dependency-injection-principles-practices-patterns)
- Richardson Maturity Model [https://martinfowler.com/articles/richardsonMaturityModel.html](https://martinfowler.com/articles/richardsonMaturityModel.html)
- Architectural Styles and the Design of Network-based Software Architectures (2000) [https://www.ics.uci.edu/~fielding/pubs/dissertation/fielding_dissertation.pdf](https://www.ics.uci.edu/~fielding/pubs/dissertation/fielding_dissertation.pdf)
- Fluent Validation [https://fluentvalidation.net/](https://fluentvalidation.net/)