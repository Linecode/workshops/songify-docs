# HATEOAS

## Ćwiczenia 14 - Data Shaping

Data Shaping jest bardzo poteznym mechanizmem, który pozwala klientowi na decydowanie jakie dokładnie pola encji chce wykorzystac w swojej aplikacji. Pozwala to na zwiekszenie elastycznosci endpoint-ów a takze na zmniejszenie ilosci danych przesyłanych po sieci. 

Aby zaimplementowac ten mechanizm bedziesz musiał uzyc [ExpandoObject](https://docs.microsoft.com/en-us/dotnet/api/system.dynamic.expandoobject?view=net-5.0)

Ale po kolei, w pierwszym kroku dodaj dwie ponizsze klasy do utils-ów lub stwórz oddzielny katalog `Common` na nie. 

```csharp
public static class ObjectExtensions
{
    public static ExpandoObject ShapeData<TSource>(this TSource source, string fields)
    {
        if (source == null) throw new ArgumentNullException(nameof(source));
        var dataShapedObject = new ExpandoObject();
        if (string.IsNullOrWhiteSpace(fields))
        {
            var propertyInfos = typeof(TSource)
                .GetProperties(BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            foreach (var propertyInfo in propertyInfos)
            {
                var propertyValue = propertyInfo.GetValue(source);
                ((IDictionary<string, object>)dataShapedObject)
                    .Add(propertyInfo.Name, propertyValue);
            }
            return dataShapedObject;
        }
        var fieldsAfterSplit = fields.Split(',');
        foreach (var field in fieldsAfterSplit)
        {
            var propertyName = field.Trim();
            var propertyInfo = typeof(TSource)
                .GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            if (propertyInfo == null)
            {
                throw new Exception($"Property {propertyName} wasn't found " +
                                    $"on {typeof(TSource)}");
            }
            var propertyValue = propertyInfo.GetValue(source);
            ((IDictionary<string, object>)dataShapedObject)
                .Add(propertyInfo.Name, propertyValue);
        }
        return dataShapedObject;
    }
}
```

```csharp
public static class EnumerableExtensions
{
    public static IEnumerable<ExpandoObject> ShapeData<TSource>(this IEnumerable<TSource> source, string fields)
    {
        if (source == null) throw new ArgumentNullException(nameof(source));
        var expandoObjectList = new List<ExpandoObject>();
        var propertyInfoList = new List<PropertyInfo>();

        if (string.IsNullOrWhiteSpace(fields))
        {
            var propertyInfos = typeof(TSource)
                .GetProperties(BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            propertyInfoList.AddRange(propertyInfos);
        }
        else
        {
            var fieldsAfterSplit = fields.Split(',');
            foreach (var field in fieldsAfterSplit)
            {
                var propertyName = field.Trim();
                var propertyInfo = typeof(TSource)
                    .GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                if (propertyInfo == null)
                    throw new Exception($"Property {propertyName} wasn't found on" +
                                        $" {typeof(TSource)}");
                propertyInfoList.Add(propertyInfo);
            }
        }

        foreach (TSource sourceObject in source)
        {
            var dataShapedObject = new ExpandoObject();
            foreach (var propertyInfo in propertyInfoList)
            {  
                var propertyValue = propertyInfo.GetValue(sourceObject);
                ((IDictionary<string, object>)dataShapedObject)
                    .Add(propertyInfo.Name, propertyValue);
            }
            expandoObjectList.Add(dataShapedObject);
        }
        return expandoObjectList;
    }
}
```

Klasa `ObjectExtensions` daje mozliwosc manipulacji ksztaltem danych na pojdeynczych obiektach. Natomiast klasa `EnumerableExtensions` daje mozliwosc manipulacji ksztalem danych kolekcji elementów. 

::: warning
Uwaga! W przypadku EnumerableExtensions kolekcja musi byc typu IEnumerable inaczej metoda pomocnicza `ShapeData` bedzie uruchomiona z klasy `ObjectExtensions` poniewaz np. typ List dziedziczy po System.Object
::: 

Następnie w wybranym przez siebie endpoincie dodaj paramter `[FromQuery] string fields` lub pobierz ta zmienna za pomoca `HttpContext.Request.Query["fields"]`.

Dalej wykorzystaj metodę rozszerzającą `ShapeData` na obiekcie klasy `Artist` lub `IEnumerable<Artist>` aby zmienic kształ danych.

::: details

```csharp
[HttpGet]
[Route("{id}", Name = nameof(GetSingle))]
public ActionResult GetSingle(int id, [FromQuery] string fields)
{
    var artist = _repository.Get(id);
    var result = artist.ShapeData(fields);
    
    return Ok(result);
}
```

:::

## Ćwiczenie 15 - HATEOAS

Do kontrolera `ArtistsController` w konstruktorze przekaz instancję obiektu `LinkGenerator`. Ten obiekt ułatwi Tobie później generowanie adresów url dla danych zasobów. 

::: details 

```csharp
private readonly IArtistsRepository _repository;
private readonly LinkGenerator _linkGenerator;

public ArtistsController(IArtistsRepository repository, LinkGenerator linkGenerator)
{
    _repository = repository;
    _linkGenerator = linkGenerator;
}
```

:::

Dodaj do projektu klasę `LinkDto`, która będzie hermetyzowac dane potrzebne do wyswietlanie hipermedi-ów w odpowiedzi z API.

```csharp
public class LinkDto
{
    public string Href { get; private set; }
    public string Rel { get; private set; }
    public string Method { get; private set; }
    
    public LinkDto(string href, string rel, string method)
    {
        Href = href;
        Rel = rel;
        Method = method;
    }
}
```


Nastepnie utwórz metodę pomocniczą `IEnumerable<LinkDto> CreateLinksForArtist(int id)`. Wklej do niej ponizszy kod. 

```csharp
private IEnumerable<LinkDto> CreateLinksForArtist(int id)
{
    var links = new List<LinkDto>
    {
        new LinkDto(_linkGenerator.GetUriByAction(HttpContext, nameof(GetSingle), values: new {id}),
            "self",
            "GET"),
        new LinkDto(_linkGenerator.GetUriByAction(HttpContext, nameof(Delete), values: new {id}),
            "delete",
            "DELETE"),
        new LinkDto(_linkGenerator.GetUriByAction(HttpContext, nameof(Update), values: new {id}),
            "update",
            "PUT")
    };

    return links;
}
```

Przyda się takze metoda do tworzenia link-ów dla kolekcji elementów. 

```csharp
private IEnumerable<LinkDto> CreateLinksForArtists()
{
    var links = new List<LinkDto>
    {
        new LinkDto(_linkGenerator.GetUriByAction(HttpContext, nameof(Get)),
            "self",
            "GET")
    };

    return links;
}
```

::: tip
To własnie w metodzie powyzej mozesz zawrzec informacje na temat paginacji. Na przykład

```csharp
private IEnumerable<LinkDto> CreateLinksForArtists(int page, int perPage)
{
    var links = new List<LinkDto>
    {
        new LinkDto(_linkGenerator.GetUriByAction(HttpContext, nameof(Get)),
            "self",
            "GET")
    };
    
    if (hasNextPage)
        links.Add(new LinkDto(_linkGenerator.GetUriByAction(HttpContext, nameof(Get), values: new { page = page + 1, perPage }),
            "next_page",
            "GET"))
            
    if (hasPrevPage)
        links.Add(new LinkDto(_linkGenerator.GetUriByAction(HttpContext, nameof(Get), values: new { page = page - 1 , perPage }),
            "prev_page",
            "GET"))

    return links;
}
```
:::

Następnie w metodach kontrolera dodaj kod, ktory będzie dodawal element "links" do odpowiedzi serwera.
Zrób to zarówno dla pojedyńczego zasobu jak i dla calej listy zasobów zgodnie z ponizszymi przykładami.

```csharp
[HttpPost]
public IActionResult Create([FromBody] Artist artist)
{
    _repository.Add(artist);

    var shapedArtist = artist.ShapeData(string.Empty);
    ((IDictionary<string, object>)shapedArtist)
        .Add("links", CreateLinksForArtist(artist.Id));

    return CreatedAtRoute(nameof(GetSingle), new {Id = artist.Id}, shapedArtist);
}
```

```csharp
[HttpGet(Name = nameof(Get))]
public IActionResult Get([FromQuery] string fields)
{
    var artists = _repository.GetAll();
    
    var shapedArtistsWithLinks = artists.ShapeData(fields)
        .Select(artist =>
    {
        var artistDictionary = artist as IDictionary<string, object>;
        var authorsLinks = CreateLinksForArtist((int) artistDictionary["Id"]);
        artistDictionary.Add("links", authorsLinks);
        return artistDictionary;
    });

    var links = CreateLinksForArtists();

    var linkedCollectionResource = new
    {
        value = shapedArtistsWithLinks,
        links
    };
    
    return Ok(linkedCollectionResource);
}
```