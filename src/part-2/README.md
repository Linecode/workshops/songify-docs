# Ćwiczenia Zaawansowane WebApi

## Ćwiczenie 10 - Cohesion

Doinstaluj do projektu dwie paczki nuget. 

- Ardalis.ApiEndpoints v3.0.0
- Swashbuckle.AspNetCore.Annotations v6.0.7

Przekształc strukture projektu tak aby pogrupowac zmieniające się rzeczy w jedno miejsce. 
Po zakończeniu transformacji struktura projektu powinna mniej więcej wyglądac jak na obrazku ponizej.

![Structure](./structure.png)

::: tip
Pamiętaj aby przy wydzielaniu metod kontrolera do osobnych klas wykorzystac biblioteke `Ardalis.ApiEndpoint` zgodnie z tym jak to było zaprezentowane na prezentacji
:::

Ponizej znajdziesz przykladowy kod dla metody Get:

```csharp
public class Get : BaseAsyncEndpoint
        .WithRequest<int>
        .WithResponse<Artist>
{
    private readonly IArtistsRepository _artistsRepository;

    public Get(IArtistsRepository artistsRepository)
    {
        _artistsRepository = artistsRepository;
    }
    
    [HttpGet("/api/artists/{id}")]
    [HttpCacheExpiration(CacheLocation = CacheLocation.Public, MaxAge = 600)]
    [HttpCacheValidation]
    [SwaggerOperation(
        Summary = "Get a specific Artist",
        Description = "Get a specific Artist",
        OperationId = "Artist.Get",
        Tags = new []{ "ArtistsEndpoint" })
    ]
    public override async Task<ActionResult<Artist>> HandleAsync(int id, CancellationToken cancellationToken = new())
    {
        var artist = await _artistsRepository.Get(id);

        if (artist is null)
            return NotFound();
        
        return Ok(artist);
    }
}
```

::: tip
Pamietaj ze jezeli chcesz zwrocic tylko sam kod odpowiedzi HTTP to mozesz dziedziczyc po klasie

```csharp
BaseAsyncEndpoint.WithRequest<int>.WithoutResponse;
```
:::

## Ćwiczenie 11 - Json Patch

Zainstaluj ponizszą paczkę nuget, która do aplikacji doda obsługę standardu JSON Patch.

- Microsoft.AspNetCore.JsonPath v5.0.3

Następnie dodaj kolejną metodę w API, która pozwoli na częsciową modyfikacje obiektu Artist.

::: tip
Aby otrzymac url endpoint-a PATCH /artists/{id} i odczytac json-a z body będziesz musiał sam wyciągnąc wartosc `id` z adresu url. Mozesz to zrobic za pomocą kodu: 

```csharp
var id = Convert.ToInt32(HttpContext.Request.RouteValues["id"]?.ToString());
```
:::

Ponizej rozwiazanie: 

::: details
```csharp
public override async Task<ActionResult> HandleAsync(JsonPatchDocument<Artist> request, CancellationToken cancellationToken = new ())
{
    // We can grab this value from RouteValues dictionary or to merge it with JsonPathDocument
    var id = Convert.ToInt32(HttpContext.Request.RouteValues["id"]?.ToString());

    var artist = await _repository.Get(id);

    if (artist is null) return NotFound();
    
    request.ApplyTo(artist, ModelState);

    if (!ModelState.IsValid)
    {
        return BadRequest(ModelState);
    }
    
    _repository.Update(artist);
    await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);

    return Ok();
}
```
:::

## Ćwiczenie 12 - Child Resource

Wykorzystując zebraną wiedzę utwórz nowy element API - Albums. Taka sekcja API powinna składa się z 3 endpointów: 

- Create -> Tworzy nowy album dla danego artysty (pamiętaj o wymogu aby przesłac id artysty) `POST api/albums`
- Get(AlbumId) -> Pobiera informacje o podanym albumie `GET api/albums/{id}`
- GetAllFromArtist -> Pobiera wszystkie albumy dla artysty o podanym Id `GET api/artists/{artistId}/albums`

Ponizej znajdziesz proponowana przezemnie strukture dla sekcji odpowiedzialnej za zarzadzanie albumami.

![Structure](./child-resource-structure.png)

Ponizej znajdziesz rozwiazanie zadania

::: details

```csharp
[HttpGet("api/artists/{artistId}/albums", Name = RouteName)]
[SwaggerOperation(
    Summary = "Get All Albums from a specific Artist",
    Description = "Get All Albums from a specific Artist",
    OperationId = "Album.GetAllFromArtists",
    Tags = new []{ "AlbumsEndpoint"})]
public override async Task<ActionResult> HandleAsync(int artistId, CancellationToken cancellationToken = new ())
{
    var albums = await _repository.GetAllFromArtist(artistId);

    return Ok(albums);
}

public const string RouteName = "Albums.GetAllFromArtist";
```

```csharp
[HttpPost("api/artist/{artistId}/album", Name = RouteName)]
[SwaggerOperation(
    Summary = "Create a specific Album",
    Description = "Create a specific Album",
    OperationId = "Album.Create",
    Tags = new []{ "AlbumsEndpoint"})]
public override async Task<ActionResult> HandleAsync(CreateAlbumResource request, CancellationToken cancellationToken = new CancellationToken())
{
    // We can grab this value from RouteValues dictionary or to merge it with JsonPathDocument
    var id = Convert.ToInt32(HttpContext.Request.RouteValues["artistId"]?.ToString());

    var album = new Album
    {
        Name = request.Name,
        ArtistId = id
    };

    _repository.Add(album);
    await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);

    return CreatedAtRoute(Get.RouteName, new {id = album.Id}, album);
}

public const string RouteName = "Albums.Create";
```

```csharp
[HttpGet("api/albums/{id}", Name = RouteName)]
[SwaggerOperation(
    Summary = "Get a specific Album",
    Description = "Get a specific Album",
    OperationId = "Album.Get",
    Tags = new []{ "AlbumsEndpoint"})]
public override async Task<ActionResult> HandleAsync(int id, CancellationToken cancellationToken = new CancellationToken())
{
    var album = await _repository.Get(id);

    if (album is null)
        return BadRequest();

    return Ok(album);
}

public const string RouteName = "Albums.Get";
```

:::


## Ćwiczenie 13 - CORS

W pliku `Startup.cs` w metodzie `ConfigureServices` dodaj ponizszy kod odpowiedzialny za podstawowa konfiguracje zabezpieczenia CORS. 

```csharp
services.AddCors(options =>
{
    options.AddPolicy("Cors Policy",
        builder =>
        {
            builder.WithOrigins("https://example.com")
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials();
        });
});
```

Następnie w metodzie `Configure` dodaj middleware CORS-a do pipeline aplikacji pomiędzy `UseRouting` a `UseAuthentication`

```csharp
app.UseCors("Cors Policy");
```